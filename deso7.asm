# Author: Doan Anh Tu
# Create date: 14/3/2016
# Hanoi university of science and technology.

.data
	messageNhap: .asciiz "Hay nhap vao mot so nguyen trong khoang 1-10: "
	khoangTrang: .asciiz " "
	enter: .asciiz "\n"
.text
nhapDoCao: # Nhap vao do cao cua hinh tam giac
	li $v0,4
	la $a0,messageNhap # load adrress
	syscall #in thong bao
	li $v0,5
	syscall # read interger
	addi $t0,$v0,0 # $t0 la gia tri nhap vao.
	addi $t3,$t3,1
	slt $t1,$t0,$t3 # n<1 then $t1=1 else $t1=0
	bne $t1,$zero,nhapDoCao #n<0 then nhapDocao
	
	li $t2,10
	slt $t1,$t0,$t2 # n<10 then $t1=1 else $t1=0
	beq $t1,$zero,nhapDoCao # n > 10 then nhapDoCao
	j process
	nop
	
process:	
	subi $s1,$t0,1 # bien de check step = n-1
loop:  #in khoang trang
	beq $s1,$s0,print # i = n-1
	nop
	addi $s0,$s0,1 # i=i+1 (step)
	li $v0,4
	la $a0,khoangTrang
	syscall
	j loop
	nop
print:
	addi $s4,$s4,1
	addi $s2,$s4,0 # so thu tu dong.
loopprint1: # in nua dong truoc
	li $v0,1
	li $a0,0 #reset
	addi $s3,$s3,1 # so dc in ra console
	add $a0,$a0,$s3
	syscall
	subi $s2,$s2,1
	beq $s2,$zero,loopprint2 #in xong mot nua dong roi chuyen sang in nua dong con lai.
	nop
	j loopprint1 # quay lai in tiep voi gia tri s3+=1.
	nop
loopprint2: # in nua dong sau
	li $v0,1
	li $a0,0 # reset
	subi $s3,$s3,1 # $s3 tro thanh so can in ra trong nua dong con lai
	add $a0,$a0,$s3
	beq $s3,$zero,continue
	nop
	syscall
	j loopprint2
	nop
	
continue:
	li $v0,4 # xuong dong
	la $a0,enter
	syscall
	
	subi $s1,$s1,1 # khi $s1 con lon hon 0 thi tiep tuc in khoang trang vao dau moi dong.
	slt $t1,$s1,$zero # $s1<0 then $t1=1 else $t1=0
	bne $t1,$zero,end
	li $s0,0 # reset i=0
	j loop
	nop
end:
